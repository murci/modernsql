DROP TABLE population;
CREATE TABLE population (
  sumlev                int,
  region                int,
  division              int,
  state                 int,
  name                  VARCHAR(100),
  census2010pop         INT,
  estimatesbase2010     INT,
  popestimate2010       INT,
  popestimate2011       INT,
  popestimate2012       INT,
  popestimate2013       INT,
  popestimate2014       INT,
  popestimate2015       INT,
  npopchg_2010          INT,
  npopchg_2011          INT,
  npopchg_2012          INT,
  npopchg_2013          INT,
  npopchg_2014          INT,
  npopchg_2015          INT,
  births2010            INT,
  births2011            INT,
  births2012            INT,
  births2013            INT,
  births2014            INT,
  births2015            INT,
  deaths2010            INT,
  deaths2011            INT,
  deaths2012            INT,
  deaths2013            INT,
  deaths2014            INT,
  deaths2015            INT,
  naturalinc2010        INT,
  naturalinc2011        INT,
  naturalinc2012        INT,
  naturalinc2013        INT,
  naturalinc2014        INT,
  naturalinc2015        INT,
  internationalmig2010  INT,
  internationalmig2011  INT,
  internationalmig2012  INT,
  internationalmig2013  INT,
  internationalmig2014  INT,
  internationalmig2015  INT,
  domesticmig2010       INT,
  domesticmig2011       INT,
  domesticmig2012       INT,
  domesticmig2013       INT,
  domesticmig2014       INT,
  domesticmig2015       INT,
  netmig2010            INT,
  netmig2011            INT,
  netmig2012            INT,
  netmig2013            INT,
  netmig2014            INT,
  netmig2015            INT,
  residual2010          INT,
  residual2011          INT,
  residual2012          INT,
  residual2013          INT,
  residual2014          INT,
  residual2015          INT,
  rbirth2011            DECIMAL(16, 12),
  rbirth2012            DECIMAL(16, 12),
  rbirth2013            DECIMAL(16, 12),
  rbirth2014            DECIMAL(16, 12),
  rbirth2015            DECIMAL(16, 12),
  rdeath2011            DECIMAL(16, 12),
  rdeath2012            DECIMAL(16, 12),
  rdeath2013            DECIMAL(16, 12),
  rdeath2014            DECIMAL(16, 12),
  rdeath2015            DECIMAL(16, 12),
  rnaturalinc2011       DECIMAL(16, 12),
  rnaturalinc2012       DECIMAL(16, 12),
  rnaturalinc2013       DECIMAL(16, 12),
  rnaturalinc2014       DECIMAL(16, 12),
  rnaturalinc2015       DECIMAL(16, 12),
  rinternationalmig2011 DECIMAL(16, 12),
  rinternationalmig2012 DECIMAL(16, 12),
  rinternationalmig2013 DECIMAL(16, 12),
  rinternationalmig2014 DECIMAL(16, 12),
  rinternationalmig2015 DECIMAL(16, 12),
  rdomesticmig2011      DECIMAL(16, 12),
  rdomesticmig2012      DECIMAL(16, 12),
  rdomesticmig2013      DECIMAL(16, 12),
  rdomesticmig2014      DECIMAL(16, 12),
  rdomesticmig2015      DECIMAL(16, 12),
  rnetmig2011           DECIMAL(16, 12),
  rnetmig2012           DECIMAL(16, 12),
  rnetmig2013           DECIMAL(16, 12),
  rnetmig2014           DECIMAL(16, 12),
  rnetmig2015           DECIMAL(16, 12)
);

COPY population FROM '/Users/guillermo/Downloads/NST-EST2015-alldata.csv' WITH(FORMAT CSV, HEADER true, NULL '\N');

SELECT region,
  division,
  CAST(AVG(BIRTHS2015) AS INTEGER) AS avg_births,
  CAST(AVG(DEATHS2015) AS INTEGER) AS avg_deaths
FROM population
WHERE state > 0 -- states only
GROUP BY GROUPING SETS (region, division, ());

CREATE TABLE regions (
  region INT,
  name   VARCHAR(50)
);

TRUNCATE TABLE regions;
INSERT INTO regions VALUES (NULL, 'Not applicable');
INSERT INTO regions VALUES (0, '-');
INSERT INTO regions VALUES (1, 'Northeast');
INSERT INTO regions VALUES (2, 'Midwest');
INSERT INTO regions VALUES (3, 'South');
INSERT INTO regions VALUES (4, 'West');

CREATE TABLE divisions (
  division INT,
  name     VARCHAR(50)
);

TRUNCATE TABLE divisions;
INSERT INTO divisions VALUES (NULL, 'Not applicable');
INSERT INTO divisions VALUES (0, '-');
INSERT INTO divisions VALUES (1, 'New England');
INSERT INTO divisions VALUES (2, 'Middle Atlantic');
INSERT INTO divisions VALUES (3, 'East North Central');
INSERT INTO divisions VALUES (4, 'West North Central');
INSERT INTO divisions VALUES (5, 'South Atlantic');
INSERT INTO divisions VALUES (6, 'East South Central');
INSERT INTO divisions VALUES (7, 'West South Central');
INSERT INTO divisions VALUES (8, 'Mountain');
INSERT INTO divisions VALUES (9, 'Pacific');

CREATE TABLE population_old AS SELECT * FROM population;


package com.buntplanet.formacion.modernsql;

import java.sql.Connection;

class BatchInsertsIndex {
  public static void main(String[] args) throws Throwable {
    Connection conn = Setup.getSingleConnection();
    Setup.prepareDBNoIndexes(conn);
    Setup.createIndexes(conn);
    InsertModes.batchInserts(1000, conn);
    conn.close();
  }
}

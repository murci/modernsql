package com.buntplanet.formacion.modernsql;

import java.sql.Connection;

public class IndividualInsertsNoPreparedStatement {
  public static void main(String[] args) throws Throwable {
    Connection conn = Setup.getSingleConnection();

    Setup.prepareDBNoIndexes(conn);

    InsertModes.individualInsertsNoPS(conn);

    conn.close();
  }
}
